<?php
/**
 * @file
 * Theme file to implements an interactive Tic Tac Toe game.
 */
?>
<div id="tic-tac-toe-container">
  <div id="tic-tac-toe-debug"></div>
  <div id="tic-tac-toe-board"></div>
</div>
