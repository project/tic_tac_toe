-- SUMMARY --

This module implements an interactive Tic Tac Toe game
It can present a Tic-Tac-Toe board that is updated using AJAX requests upon 
each user move.

The module implements the alpha beta search algorithm (Minimax search optimized 
by performing alpha beta pruning) for determining the next move, which are 
common algorithms for zero-sum two player games (e.g. Tic Tac Toe and Chess).

The module can also display the statistics in a Web page.

-- INSTALLATION --

Download raphael-min.js from http://drupal.org/files/raphael-min.js_.txt and 
rename it to raphael-min.js and put into the directory
sites/all/libraries/tic_tac_toe

Install the libaries api module ( http://drupal.org/project/libraries )

Install the Tic Tac Toe module
